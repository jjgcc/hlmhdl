# hlmhdl

High Level Modeling Integration for Hardware Description Languages

## Run It!

To run the example provided, do the following:

- Compile dependencies.

    ```
    make
    ```

- Set up Modelsim project.

    ```
    vlog include/dpic.sv sv/example.sv
    ```

- Run Python manager script.

    ```
    ./dtor.py
    ```

- Run Modelsim simulation.

    ```
    vsim -onfinish stop example_tb -novopt -sv_lib dpic
    ```

You should see the Modelsim simulation environment at this stage along some
python messages from the manager script.

Now you are ready to run a couple of cycles with the Modelsim command:


```
run 10ns
```
