# coding=utf-8
__all__ = ["ZMQSocket"]

from .messages import Envelope
from .handler import Handler
from .print import pdebug

import os
import zmq


class ZMQSocket(object):

    def __init__(self, uuid: str, wdir: str) -> None:

        socket_name = "{}-dtor.ipc".format(uuid)
        socket_path = os.path.join(wdir, socket_name)
        self.__ctx = zmq.Context()
        self.__socket = self.__ctx.socket(zmq.REP)
        pdebug('Bind to socket {}'.format(socket_path),
            self.__class__.__name__, self.__init__.__name__)
        self.__socket.bind("ipc://{}".format(socket_path))

    def recv(self) -> None:
        envelope_bin = self.__socket.recv()

        envelope = Envelope()
        envelope.ParseFromString(envelope_bin)

        reply = Handler.reply(envelope)
        reply = reply.SerializeToString() if reply is not None else b''
        
        self.__socket.send(reply)

        Handler.call(envelope)

    def term(self):
        self.__socket.close()
        self.__ctx.term()
