# coding=utf-8
__all__ = ['print_debug', 'pdebug', 'pinfo', 'pwarn', 'perror', 'pecho',\
           'IncrPositionBar', 'DecrAmountBar', 'IncrAmountBar']

from click import echo, secho, progressbar

__debug = False

def print_debug(flag: bool) -> None:
    global __debug
    __debug = flag

def pdebug(msg, cname: str = None, fname: str = None, nl: bool = True) -> None:
    global __debug
    if __debug:
        echo('[', nl=False)
        secho('DEBUG', nl=False, fg='blue')
        echo('] ', nl=False)
        if cname is not None:
            echo('{}'.format(cname), nl=False)
        if fname is not None:
            echo('::{}: '.format(fname), nl=False)
        elif cname is not None:
            echo(' ', nl=False)
        echo(msg, nl=nl)

def pecho(msg, nl: bool = True) -> None:
    echo(msg, nl=nl)

def pinfo(msg, nl: bool = True) -> None:
    echo('[', nl=False)
    secho('INFO', nl=False, fg='green')
    echo('] ', nl=False)
    echo(msg, nl=nl)

def pwarn(msg, nl: bool = True) -> None:
    echo('[', nl=False)
    secho('WARN', nl=False, fg='yellow')
    echo('] ', nl=False)
    echo(msg, nl=nl)

def perror(msg, nl: bool = True) -> None:
    echo('[', nl=False)
    secho('ERROR', nl=False, fg='red')
    echo('] ', nl=False)
    echo(msg, nl=nl)

class IncrPositionBar(object):
    def __init__(self, size: int) -> None:
        assert size > 0

        self.__size = size
        self.__pos = -1
        self.__npos = -1
        self.__bar = progressbar(length=size,
                                 item_show_func=lambda i: str(self.__npos+1))

    def update(self, pos: int) -> None:
        assert pos < self.__size
        
        if pos > self.__pos:
            self.__npos = pos
            self.__bar.update(pos-self.__pos)
            self.__pos = pos

        if self.__pos == (self.__size-1):
            secho('  pass', fg='green', bold=True)

class DecrAmountBar(object):
    def __init__(self, length: int, label: str = None) -> None:
        assert length > 0

        self.__remaining = length
        self.__nremaining = length
        self.__bar = progressbar(length=length,
                                 item_show_func=lambda i: str(self.__nremaining),
                                 label=label)

    def update(self, remaining: int) -> None:
        assert remaining <= self.__remaining
        
        if remaining < self.__remaining:
            self.__nremaining = remaining
            self.__bar.update(self.__remaining - remaining)
            self.__remaining = remaining

        if not self.__remaining:
            secho('  pass', fg='green', bold=True)

class IncrAmountBar(object):
    def __init__(self, size: int, label: str = None) -> None:
        assert size > 0

        self.__size = size
        self.__amount = 0
        self.__namount = 0 
        self.__bar = progressbar(length=size,
                                 item_show_func=lambda i: str(self.__namount),
                                 label=label)

    def update(self, amount: int) -> None:
        assert amount in range(self.__amount, self.__size+1)
        
        if amount > self.__amount:
            self.__namount = amount 
            self.__bar.update(amount - self.__amount)
            self.__amount = amount 

        if self.__amount == self.__size:
            secho('  pass', fg='green', bold=True)
