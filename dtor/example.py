# coding=utf-8
__all__ = ["Example"]

from .handler import Handler
from .messages import FooMsg, BarMsg, AckMsg 
from .print import pdebug, pinfo, pwarn, perror

class Example(object):
    def __init__(self):
        Handler.register_reply('foo', self.foo)
        Handler.register_reply('bar', self.bar)

    def foo(self, _: str, foo: FooMsg) -> AckMsg:
        pdebug('Received value {}, send ack'.format(foo.data),
            self.__class__.__name__, self.foo.__name__)
        ack = AckMsg()
        ack.data = foo.data
        return ack

    def bar(self, _: str, bar: BarMsg) -> AckMsg:
        pdebug('Received value {}, send ack'.format(bar.data),
            self.__class__.__name__, self.bar.__name__)
        ack = AckMsg()
        ack.data = bar.data
        return ack
