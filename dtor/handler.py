# coding=utf-8
__all__ = ["Handler"]

from typing import Callable, Any
from collections import defaultdict
from typing import Tuple

from .messages import Envelope
from .print import pdebug, pinfo, pwarn, perror

Coordinate = Tuple[int, int]

unhandled_call = set()
unhandled_reply = set()

def default_call_handler(request, _):
    unhandled_call.add(request)

def default_reply_handler(request, _):
    unhandled_reply.add(request)

def default_module_reply_handler(request, _):
    pass

default_call_chain = set()
default_call_chain.add(default_call_handler)
call_handler = defaultdict(lambda: set(default_call_chain))
module_call_handler = defaultdict(lambda: defaultdict(lambda: set(default_call_chain)))

reply_handler = defaultdict(lambda: default_reply_handler)
module_reply_handler = defaultdict(lambda: defaultdict(lambda: default_module_reply_handler))

class Handler:
    @staticmethod
    def envelope_field_check(field_name: str) -> None:
        Envelope().HasField(field_name)

    @staticmethod
    def register_handler(request: str,
            handler: Callable[[str, object], Any]) -> None:
        Handler.envelope_field_check(request)
        pdebug('Register request: {}, handler {}'.format(request, str(handler)),\
            __class__.__name__, Handler.register_handler.__name__)
        call_handler[request].add(handler)
        if default_call_handler in call_handler[request]:
            call_handler[request].remove(default_call_handler)

    @staticmethod
    def clear_handler(request: str,
            handler: Callable[[str, object], Any]) -> None:
        Handler.envelope_field_check(request)
        pdebug('Register request: {}, handler {}'.format(request, str(handler)),\
            __class__.__name__, Handler.clear_handler.__name__)
        if handler in call_handler[request]:
            call_handler[request].remove(handler)

    @staticmethod
    def register_module_handler(cid: Coordinate, request: str,
            handler: Callable[[str, object], Any]) -> None:
        Handler.envelope_field_check(request)
        pdebug('Register request: {}, handler {}'.format(request, str(handler)),\
            __class__.__name__, Handler.register_module_handler.__name__)
        module_call_handler[cid][request].add(handler)
        if default_call_handler in module_call_handler[cid][request]:
            module_call_handler[cid][request].remove(default_call_handler)

    @staticmethod
    def register_reply(request: str,
            handler: Callable[[str, object], Any]) -> None:
        Handler.envelope_field_check(request)
        for cid in module_reply_handler:
            assert module_reply_handler[cid][request] is default_module_reply_handler,\
                'Module request "{}" already registered by {}'.format(request, cid)
        pdebug('Register reply: {}, handler {}'.format(request, str(handler)),\
            __class__.__name__, Handler.register_reply.__name__)
        if reply_handler[request] is not default_reply_handler:
           pwarn('{}::{}: Overwriting previous handler registered {}'\
                    .format(self.__class__.__name__,\
                            self.register_reply.__name__,\
                            str(reply_handler[request])))
        reply_handler[request] = handler

    @staticmethod
    def register_module_reply(cid: Coordinate, request: str,
            handler: Callable[[str, object], Any]) -> None:
        Handler.envelope_field_check(request)
        assert reply_handler[request] is default_reply_handler,\
            'Global request "{}" already registered'.format(request)
        pdebug('Register reply: {}, handler {}'.format(request, str(handler)),\
            __class__.__name__, Handler.register_module_reply.__name__)
        if module_reply_handler[cid][request] is not default_module_reply_handler:
           pwarn('{}::{}: Overwriting previous handler registered {} by module {}'\
                    .format(self.__class__.__name__,\
                            self.register_module_reply.__name__,\
                            str(reply_handler[request]),
                            cid))
        module_reply_handler[cid][request] = handler

    @staticmethod
    def call(msg: Envelope) -> Any:
        req = msg.WhichOneof('payload')
        assert req is not None

        pdebug('request: {}'.format(req), __class__.__name__,\
            Handler.call.__name__)
        data = getattr(msg, req)

        # Call to module handlers

        # Call to non-module handlers
        for f in call_handler[req]:
            f(req, data)

    @staticmethod
    def reply(msg: Envelope) -> Any:
        req = msg.WhichOneof('payload')
        assert req is not None

        pdebug('request: {}'.format(req), __class__.__name__,\
            Handler.reply.__name__)
        data = getattr(msg, req)
        
        return reply_handler[req](req, data)
