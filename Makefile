DIRECTOR = dtor
CONNECTOR = libdtor

CC=gcc
CFLAGS = -c -O2 -Wall -Wextra -Iinclude -fPIC -fvisibility=hidden
LDFLAGS = -shared -lprotobuf-c -lzmq -fvisibility=hidden

MESSAGES = $(wildcard common/*.proto)
PYMSGSFLD=$(DIRECTOR)/messages_pb2
CMSGSFLD=$(CONNECTOR)/messages

MSGCC_PY=protoc -Icommon --python_out=$(PYMSGSFLD)
PYMESSAGES = $(MESSAGES:common%.proto=$(PYMSGSFLD)/%_pb2.py)

MSGCC_C=protoc-c -Icommon --c_out=$(CMSGSFLD)
CMESSAGES = $(MESSAGES:common%.proto=$(CMSGSFLD)/%.pb-c.c)
HMESSAGES = $(MESSAGES:common%.proto=$(CMSGSFLD)/%.pb-c.h)

SOURCES=$(wildcard $(CONNECTOR)/*.c) $(CMESSAGES)
HEADERS=$(wildcard $(CONNECTOR)/*.h) $(HMESSAGES)

OBJECTS=$(SOURCES:.c=.o)
LIBRARY=dpic.so

all: $(PYMESSAGES) $(CMESSAGES) $(SOURCES) $(HEADERS) $(LIBRARY)

$(PYMSGSFLD):
	mkdir $@

$(CMSGSFLD):
	mkdir $@

$(PYMESSAGES): $(PYMSGSFLD) $(MESSAGES)
	$(MSGCC_PY) $(MESSAGES)
	
$(HMESSAGES) $(CMESSAGES): $(CMSGSFLD) $(MESSAGES)
	$(MSGCC_C) $(MESSAGES)
	
$(LIBRARY): $(OBJECTS)
	$(CC) $(OBJECTS) $(LDFLAGS) -o $@

depend: .depend

.depend: $(SOURCES)
	rm -f ./.depend
	$(CC) $(CFLAGS) -MM $^ >> ./.depend

include .depend

.PHONY: clean clear

clean clear:
	rm -f $(OBJECTS) $(LIBRARY) $(CMESSAGES) $(PYMESSAGES) ./.depend

