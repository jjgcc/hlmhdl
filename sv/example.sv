`timescale 1ns / 1ps

module example import dpic::foo_msg_t, dpic::bar_msg_t, dpic::ack_msg_t,
    dpic::send_foo, dpic::send_bar;
(
    clk_i
);

    input clk_i;

    foo_msg_t foo;
    bar_msg_t bar;
    ack_msg_t ack;

    always @(posedge clk_i) begin
        int rcode;

        foo.data = 3;
        $display("[INFO] %m::send_foo() send %d", foo.data);
        rcode = send_foo(foo, ack);
        if (rcode < 0) begin
            $display("[ERROR] %m::send_foo() returned %d", rcode);
            $finish(1);
        end
        else begin
            $display("[INFO] %m::send_foo() acknowledge %d", ack.data);
        end

        bar.data = 7;
        $display("[INFO] %m::send_bar() send %d", bar.data);
        rcode = send_bar(bar, ack);
        if (rcode < 0) begin
            $display("[ERROR] %m::send_foo() returned %d", rcode);
            $finish(1);
        end
        else begin
            $display("[INFO] %m::send_bar() acknowledge %d", ack.data);
        end

    end
endmodule

module example_tb import dpic::connect;;

    reg clk;

    example uut(clk);

    initial begin
        int rcode;
        $display("[INFO] %m:: Connecting to socket", rcode);
        rcode = connect("/tmp/", "example_tb");
        if (rcode < 0) begin
            $display("[ERROR] %m::connect() returned %d", rcode);
            $finish(1);
        end
        else begin
            $display("[INFO] %m::connect() returned %d", rcode);
        end

        clk <= 1'b0;
    end

    always
        #1 clk <= ~clk;
endmodule
