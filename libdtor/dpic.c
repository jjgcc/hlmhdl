#include <zmq.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <stdint.h>
#include <assert.h>
#include <dpic.h>
#include "errors.h"
#include "messages/messages.pb-c.h"
#include "internal.h"

#define API __attribute__((visibility("default")))

/* ZMQ endpoint */
static const char transport_[] = "ipc://";
static const char endpoint_suffix_[] = "-dtor.ipc";
static const size_t transport_len_ = sizeof(transport_);
static const size_t endpoint_suffix_len_ = sizeof(endpoint_suffix_);

static int send_recv(size_t send_len)
{
    assert(socket_ != NULL);

    int err;
    int retry = 0;
    do {
        err = zmq_send(socket_, data_buff_, send_len, 0);
        retry++;
    } while (err == -1  && errno == EINTR && retry < SEND_RECV_MAX_RETRY);
    if (err == -1) DTOR_ERROR_RAISE(DTOR_E_ZMQ);

    memset(data_buff_, 0, MAX_BUFFER_SIZE); // Just in case
    retry = 0;
    int data_size;
    do {
        data_size = zmq_recv(socket_, data_buff_, MAX_BUFFER_SIZE, 0);
        retry++;
    } while (data_size == -1 && errno == EINTR && retry < SEND_RECV_MAX_RETRY);
    if (data_size == -1) DTOR_ERROR_RAISE(DTOR_E_ZMQ);
    if (data_size > MAX_BUFFER_SIZE) DTOR_ERROR_RAISE(DTOR_E_TR);

    return data_size;

DTOR_ERROR_CATCH
    return DTOR_RET_FAIL;
}

/******************************************** PUBLIC API **********************************************/
int API connect(const char *wrk_dir, const char *uuid)
{
    char *endpoint = NULL;
    if (!wrk_dir || !uuid) DTOR_ERROR_RAISE(DTOR_E_IP);
    if (context_ || socket_) DTOR_ERROR_RAISE(DTOR_E_AC);

    const size_t uuid_len = strlen(uuid);
    const size_t wrk_dir_len = strlen(wrk_dir);
    const size_t endpoint_len = transport_len_ + wrk_dir_len + uuid_len + endpoint_suffix_len_ + 1;
    endpoint = calloc(1, endpoint_len);
    if (!endpoint)  DTOR_ERROR_RAISE(DTOR_E_STD);

    int ret = snprintf(endpoint, endpoint_len, "%s%s%s%s", transport_, wrk_dir, uuid, endpoint_suffix_);
    if (ret <= 0) DTOR_ERROR_RAISE(DTOR_E_EP);

    context_ = zmq_ctx_new ();
    if (!context_) DTOR_ERROR_RAISE(DTOR_E_NC);

    socket_ = zmq_socket (context_, ZMQ_REQ);
    if (!socket_)  DTOR_ERROR_RAISE(DTOR_E_ZMQ);

    ret = zmq_connect(socket_, endpoint);
    if (ret == -1)  DTOR_ERROR_RAISE(DTOR_E_ZMQ);
    free(endpoint);

    return DTOR_RET_OK;
DTOR_ERROR_CATCH
    free(endpoint);
    return DTOR_RET_FAIL;
}

__attribute__((destructor))
int API dpic_close()
{
    if(socket_ && zmq_close(socket_) == -1)  DTOR_ERROR_RAISE(DTOR_E_ZMQ);
    socket_ = NULL;

    if(context_ && zmq_ctx_term(context_) == -1)  DTOR_ERROR_RAISE(DTOR_E_ZMQ);
    context_ = NULL;

    return DTOR_RET_OK;
DTOR_ERROR_CATCH
    return DTOR_RET_FAIL;
}

int API send_foo(const foo_msg_t *foo, ack_msg_t *ack)
{
    if (!foo || !ack) DTOR_ERROR_RAISE(DTOR_E_IP);

    envelope_set_foo(foo);
    size_t envelope_size = envelope_pack();

    int recv_data_size = send_recv(envelope_size);
    if (recv_data_size == DTOR_RET_FAIL) DTOR_ERROR_RAISE(DTOR_E_ZMQ);

    AckMsg *ack_msg;
    ack_msg = ack_msg__unpack(NULL, (size_t) recv_data_size, data_buff_);

    *ack = (ack_msg_t) {
        .data      = (ack_msg->has_data)? ack_msg->data : 0,
    };

    ack_msg__free_unpacked(ack_msg, NULL);

    return DTOR_RET_OK;
DTOR_ERROR_CATCH
    return DTOR_RET_FAIL;
}

int API send_bar(const bar_msg_t *bar, ack_msg_t *ack)
{
    if (!bar || !ack) DTOR_ERROR_RAISE(DTOR_E_IP);

    envelope_set_bar(bar);
    size_t envelope_size = envelope_pack();

    int recv_data_size = send_recv(envelope_size);
    if (recv_data_size == DTOR_RET_FAIL) DTOR_ERROR_RAISE(DTOR_E_ZMQ);

    AckMsg *ack_msg;
    ack_msg = ack_msg__unpack(NULL, (size_t) recv_data_size, data_buff_);

    *ack = (ack_msg_t) {
        .data      = (ack_msg->has_data)? ack_msg->data : 0,
    };

    ack_msg__free_unpacked(ack_msg, NULL);

    return DTOR_RET_OK;
DTOR_ERROR_CATCH
    return DTOR_RET_FAIL;
}
