#ifndef LIBDTOR_ERRORS_H
#define LIBDTOR_ERRORS_H
#include <zmq.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

enum dtor_err_code {
    DTOR_E_NOERR = 0,
    DTOR_E_EXIT = DTOR_E_NOERR,
    DTOR_E_STD = 1,
    DTOR_E_ZMQ = 2,
    DTOR_E_IP,
    DTOR_E_AC,
    DTOR_E_EP,
    DTOR_E_NC,
    DTOR_E_TR
};

static const char errmsg[][128] = {
        [DTOR_E_NOERR] = "",    /* Reserved */ /* No error / Error exit (no error message) */
        [DTOR_E_STD] = "",      /* Reserved */ /* Standard error */
        [DTOR_E_ZMQ] = "",      /* Reserved */ /* ZMQ error */
        [DTOR_E_IP] = "Invalid parameters.",
        [DTOR_E_AC] = "Director is already connected.",
        [DTOR_E_EP] = "Can not create endpoint name.",
        [DTOR_E_NC] = "Error creating ZMQ Context.",
        [DTOR_E_TR] = "Truncated zmq_recv."
};

#define DTOR_RET_OK 0
#define DTOR_RET_FAIL -1

static int dtor_errno;

#define DTOR_ERROR_RAISE(err) do {dtor_errno = err; goto error;} while(0)
#define DTOR_ERROR_CATCH error: if (dtor_errno != DTOR_E_NOERR) dtor_perror(__func__);

static void dtor_perror(const char *fname)
{
    if (dtor_errno == DTOR_E_NOERR) return;

    if(dtor_errno == DTOR_E_STD)
        fprintf(stderr, "[ERROR] DTOR_E_STD (%s) %s\n", fname, strerror(errno));
    else if(dtor_errno == DTOR_E_ZMQ)
        fprintf(stderr, "[ERROR] DTOR_E_ZMQ (%s) %s\n", fname, zmq_strerror(errno));
    else
        fprintf(stderr, "[ERROR] DTOR_E (%s) %s\n", fname, errmsg[dtor_errno]);

}

#endif //LIBDTOR_ERRORS_H
