#ifndef INTERNAL_H
#define INTERNAL_H
#include <stdint.h>

#include "messages/messages.pb-c.h"
#include "dpic.h"

/* ZMQ state */
static void *context_ = NULL; __attribute__ ((aligned (64)))
static void *socket_ = NULL;  __attribute__ ((aligned (64)))  // Sync socket

#define SEND_RECV_MAX_RETRY 3

/* Buffers */
#define MAX_BUFFER_SIZE 256
static uint8_t data_buff_[MAX_BUFFER_SIZE] = {0}; __attribute__ ((aligned (64)))

/* Messages */
static Envelope      envelope_ = ENVELOPE__INIT;     __attribute__ ((aligned (64)))
static FooMsg        foo_msg_ = FOO_MSG__INIT; __attribute__ ((aligned (64)))
static BarMsg        bar_msg_ = BAR_MSG__INIT; __attribute__ ((aligned (64)))

/* Envelope helpers */
static size_t envelope_pack()
{
    size_t envelope_size = envelope__get_packed_size(&envelope_);
    assert(envelope_size > 0 && envelope_size <= MAX_BUFFER_SIZE);
    memset(data_buff_, 0, MAX_BUFFER_SIZE);
    envelope__pack(&envelope_, data_buff_);
    return envelope_size;
}

static void envelope_set_foo(const foo_msg_t *foo)
{
    foo_msg__init(&foo_msg_);
    foo_msg_.has_data = 1;
    foo_msg_.data = foo->data;

    envelope__init(&envelope_);
    envelope_.payload_case = ENVELOPE__PAYLOAD_FOO;
    envelope_.foo= &foo_msg_;
}

static void envelope_set_bar(const bar_msg_t *bar)
{
    bar_msg__init(&bar_msg_);
    bar_msg_.has_data = 1;
    bar_msg_.data = bar->data;

    envelope__init(&envelope_);
    envelope_.payload_case = ENVELOPE__PAYLOAD_BAR;
    envelope_.bar= &bar_msg_;
}

#endif //INTERNAL_H
