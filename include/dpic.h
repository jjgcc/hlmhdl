#ifndef DPIC_H
#define DPIC_H
#include <stdint.h>

typedef struct {
    uint32_t data;
} foo_msg_t;

typedef struct {
    uint32_t data;
} bar_msg_t;

typedef struct {
    uint32_t data;
} ack_msg_t;

/************************************ API *************************************/
int connect(const char *wrk_dir, const char *uuid);
int send_foo(const foo_msg_t *foo, ack_msg_t *ack);
int send_bar(const bar_msg_t *bar, ack_msg_t *ack);
int dpic_close();

#endif //DPIC_H
