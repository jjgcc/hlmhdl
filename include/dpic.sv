package dpic;

    typedef struct {
        int unsigned data;
    } foo_msg_t;

    typedef struct {
        int unsigned data;
    } bar_msg_t;

    typedef struct {
        int unsigned data;
    } ack_msg_t;

/************************************ API *************************************/
    import "DPI-C" function int connect(input string wrk_dir, input string uuid);
    import "DPI-C" function int send_foo(input foo_msg_t foo, output ack_msg_t ack);
    import "DPI-C" function int send_bar(input bar_msg_t bar, output ack_msg_t ack);
    import "DPI-C" function int dpic_close();

endpackage
