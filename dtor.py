#!/usr/bin/env python3

import click
import time

from dtor.example import Example
from dtor.print import *
from dtor.socket import ZMQSocket

@click.command()
def main() -> None:
    
    print_debug(True)

    pinfo('Instantiate socket')
    socket = ZMQSocket('example_tb', '/tmp')

    pinfo('Instantiate example module')
    example = Example()

    try:
        while True:
            socket.recv()
    except KeyboardInterrupt:
        pinfo('Keyboard Interrupt')
    finally:
        socket.term()

if __name__ == '__main__':
    main()
